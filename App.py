import tkinter as tk
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

def plot(ax, canvas):
    ax.clear()
    x = np.random.randint(0, 10, 10)
    y = np.random.randint(0, 10, 10)
    ax.scatter(x, y)
    canvas.draw()


root = tk.Tk()
fig, ax = plt.subplots()
#frame
frames = tk.Frame(root)
#canvas
canvas = FigureCanvasTkAgg(fig, master=frames)
canvas.get_tk_widget().pack()
#button
button = tk.Button(frames, text="run", command=lambda: plot(ax, canvas))
button.pack()

frames.pack()


root.mainloop()
