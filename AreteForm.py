import tkinter as tk
class AreteForm (tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Nouveau Lien")
    
        tk.Label(self, text="Serveur 1:").grid(row=0, column=0, padx=5, pady=5)
        self.serveur1 = tk.Entry(self)
        self.serveur1.grid(row=0, column=1, padx=5, pady=5)

        tk.Label(self, text="Serveur 2:").grid(row=1, column=0, padx=5, pady=5)
        self.serveur2 = tk.Entry(self)
        self.serveur2.grid(row=1, column=1, padx=5, pady=5)

        tk.Label(self, text="Value: ").grid(row=2, column=0, padx=5, pady=5)
        self.value = tk.Entry(self)
        self.value.grid(row=2, column=1, padx=5, pady=5)

        self.submit_button = tk.Button(self, text="Ajouter")
        self.submit_button.grid(row=3, columnspan=2, padx=5, pady=5)
        pass
