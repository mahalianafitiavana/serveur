import tkinter as tk
class ServerForm(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Nouveau Serveur")
        
        tk.Label(self, text="Adresse IP:").grid(row=0, column=0, padx=5, pady=5)
        self.ip = tk.Entry(self)
        self.ip .grid(row=0, column=1, padx=5, pady=5)

        tk.Label(self, text="Domaine:").grid(row=1, column=0, padx=5, pady=5)
        self.domain = tk.Entry(self)
        self.domain.grid(row=1, column=1, padx=5, pady=5)

        tk.Label(self, text="Coordonnée X:").grid(row=2, column=0, padx=5, pady=5)
        self.x = tk.Entry(self)
        self.x.grid(row=2, column=1, padx=5, pady=5)

        tk.Label(self, text="Coordonnée Y:").grid(row=3, column=0, padx=5, pady=5)
        self.y = tk.Entry(self)
        self.y.grid(row=3, column=1, padx=5, pady=5)

        self.submit_button = tk.Button(self, text="Valider")
        self.submit_button.grid(row=4, columnspan=2, padx=5, pady=5)
