import tkinter as tk
from Arete import Arete
from AreteForm import AreteForm
from Graphe import Graphe
from ServerForm import ServerForm
from Serveur import Serveur
import matplotlib.pyplot as plt
import networkx as nx
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

class Window(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Serveurs")
        self.minsize(width=1000, height=800)
        self.maxsize(width=1000, height=800)

        self.frame1 = tk.Frame(self, bg="white")
        self.frame1.pack(fill=tk.BOTH)

        self.serveur_button = tk.Button(self.frame1, text="Nouveau serveur", command=self.new_server)
        self.serveur_button.pack(side="top", padx=5, pady=5)

        self.arete_button = tk.Button(self.frame1, text="Nouveau lien", command=self.new_link)
        self.arete_button.pack(side="top", padx=5, pady=5)

        #inittialiser le graphe
        self.graph = Graphe()

        # Initialiser le canvas
        self.fig, self.ax = plt.subplots()
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.frame1)
        self.canvas = tk.Canvas(self.frame1, bg="white", width=400, height=400)
        # self.canvas.mpl_connect("button_press_event", self.on_pressed)
        # self.canvas.mpl_connect("motion_notify_event", self.on_motion)
        # self.canvas.mpl_connect("button_release_event", self.on_release)
        self.draw()
        # self.canvas.get_tk_widget().pack(fill=tk.BOTH, expand=True)
        self.canvas.pack(fill=tk.BOTH, expand=True)

        #init selected serveru
        self.selected_serveur = None
        pass

    def on_pressed(self, event):
        if event.xdata is not None and event.ydata is not None:
            psi = 5
            for s in self.graph.sommets:
                if abs(s.x - event.xdata) < psi and abs(s.y - event.ydata) < psi:
                    self.selected_serveur = s
        pass

    def on_release(self, event):
        self.selected_serveur = None
        pass

    def on_motion(self, event):
        if self.selected_serveur is not None:
            new_x = event.xdata
            new_y = event.ydata
            self.graph.mettreAJourCoordonnees(self.selected_serveur, new_x, new_y)
            self.draw()
        pass

    def new_server(self):
        self.serveur_form = ServerForm(self)
        self.serveur_form.submit_button.config(command=self.addserveur)
        print("Nouveau Serveur\n")
        pass

    def new_link(self):
        self.arete_form = AreteForm(self)
        self.arete_form.submit_button.config(command=self.addarete)
        print("Nouveau lien")
        pass

    def addserveur(self):
        ip = self.serveur_form.ip.get()
        domaines = self.serveur_form.domain.get().split(";")
        x = self.serveur_form.x.get()
        y = self.serveur_form.y.get()
        serveur = Serveur(ip, domaines, float(x), float(y))
        self.graph.ajouterSommet(serveur)   # ajouter le serveur au graphe
        serveur.save()                      # sauvegarder le serveur dans dns.txt
        self.draw()
        pass

    def addarete(self):
        weight = self.arete_form.value.get()
        s1 = self.arete_form.serveur1.get()
        s2 = self.arete_form.serveur2.get()
        arete = Arete(float(weight), s1, s2)
        self.graph.ajouterArete(arete)
        self.draw()
        pass

    def draw(self):
        # Nettoyer le canevas avant de dessiner le nouveau graphe
        G, positions = self.graph.getGraphe()
        for s in self.graph.sommets:
            print(s.__str__())
        
        nodes = {}
        edges = {}
        for node, (x, y) in positions.items():
            nodes[node] = self.canvas.create_oval(x - 10, y - 10, x + 10, y + 10, fill="skyblue")
        for edge in G.edges():
            u, v = edge
            x1, y1 = positions[u]
            x2, y2 = positions[v]
            edges[edge] = self.canvas.create_line(x1, y1, x2, y2, fill="black")
        pass

window = Window()
window.mainloop()
