import networkx as nx

class Graphe(object):
    def __init__(self):
        self.sommets = []
        self.aretes = []

    def getSommets(self, serveur):
        for s in self.sommets:
            if s.ip == serveur:
                return s

    def ajouterSommet(self, s):
        self.sommets.append(s)

    def ajouterArete(self, a):
        self.aretes.append(a)

    def mettreAJourCoordonnees(self, serveur, new_x, new_y):
        for s in self.sommets:
            if s.ip == serveur.ip:
                s.x = new_x
                s.y = new_y
                break

    def getGraphe(self):
        G = nx.Graph()
        for s in self.sommets:
            G.add_node(s.ip)
        for a in self.aretes:
            G.add_edge(a.un, a.deux, weight=a.weight)
        positions = {s.ip: (s.x, s.y) for s in self.sommets}
        return G, positions
