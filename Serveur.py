class Serveur (object):
    def __init__(self, ip, domaines, x, y):
        self.ip = ip
        self.domaines = domaines
        self.distance = float('inf')
        self.voisins = []
        self.x = x
        self.y = y
        self.avaible = True

    def addVoisin(self, voisin):
        self.voisins.append(voisin)

    def setDistance(self, distance):
        self.distance = distance

    def __str__(self):
        return f"Serveur: IP={self.ip}, Domaines={self.domaines}, x={self.x}, y={self.y}, Distance={self.distance}"

    def printVoisins(self):
        for voisin in self.voisins:
            print(voisin.ip)

    def save(self):
        with open("dns.txt", "a") as filout:
            for d in self.domaines:
                filout.write(d+"="+self.ip+"\n")
