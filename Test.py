import networkx as nx
import matplotlib.pyplot as plt
import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Créer un graphe vide
G = nx.Graph()

# Ajouter des nœuds
G.add_nodes_from([1, 2, 3, 4])

# Définir les positions personnalisées des nœuds
node_positions = {1: (0, 0), 2: (1, 1), 3: (2, 0), 4: (1, -1)}

# Créer une fenêtre Tkinter
root = tk.Tk()

# Créer un canevas Tkinter pour le graphe
fig, ax = plt.subplots()
nx.draw(G, pos=node_positions, with_labels=True, node_color='skyblue', ax=ax)

# Intégrer le canevas dans la fenêtre Tkinter
canvas = FigureCanvasTkAgg(fig, master=root)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

# Lancer la boucle principale de Tkinter
root.mainloop()
