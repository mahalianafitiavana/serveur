

class Panning(object):
    def __init__(self, ax):
        self.ax = ax
        self.button_press_loc = None
        self.drag_active = False
        pass

    def press(self, event):
        if event.inaxes == self.ax:
            self.button_press_loc = event.xdata, event.ydata
            self.drag_active = True
            print("press")
        pass

    def release(self, event):
        self.drag_active = False
        print("release")

    def drag(self, event):
        if event.inaxes == self.ax and self.drag_active:
            dx = event.xdata - self.button_press_loc[0]
            dy = event.ydata - self.button_press_loc[1]
            self.button_press_loc = event.xdata, event.ydata        #mettre a jour location pressed
            self.ax.set_xlim(self.ax.get_xlim()[0] + dx, self.ax.get_xlim()[1] + dx )
            self.ax.set_ylim(self.ax.get_ylim()[0] + dy, self.ax.get_ylim()[1] + dy )
            self.ax.figure.canvas.draw()
            print("drag")
