import tkinter as tk
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

def start_drag(event):
    global selected_node, last_x, last_y
    last_x = event.x
    last_y = event.y
    for node, (x, y) in positions.items():
        if abs(x - event.x) < 10 and abs(y - event.y) < 10:
            selected_node = node

def drag(event):
    global selected_node, last_x, last_y
    if selected_node is not None:
        new_x = event.x
        new_y = event.y
        canvas.move(nodes[selected_node], new_x - last_x, new_y - last_y)
        positions[selected_node] = (new_x, new_y)
        last_x = new_x
        last_y = new_y

root = tk.Tk()
root.title("Déplacement des nœuds d'un graphe")

frame = tk.Frame(root)
frame.pack(fill=tk.BOTH, expand=True)

canvas = tk.Canvas(frame, bg="white", width=400, height=400)
canvas.pack(fill=tk.BOTH, expand=True)

G = nx.Graph()
G.add_nodes_from([1, 2, 3, 4, 5])
G.add_edges_from([(1, 2), (1, 3), (2, 4), (2, 5)])

positions = nx.spring_layout(G, seed=42)

nx.draw(G, pos=positions, with_labels=True, node_color='skyblue', node_size=500, ax=plt.gca())

nodes = {}
for node, (x, y) in positions.items():
    nodes[node] = canvas.create_oval(x - 10, y - 10, x + 10, y + 10, fill="red")

canvas.bind("<Button-1>", start_drag)
canvas.bind("<B1-Motion>", drag)

selected_node = None
last_x = 0
last_y = 0

root.mainloop()
